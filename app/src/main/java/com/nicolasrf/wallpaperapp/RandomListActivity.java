package com.nicolasrf.wallpaperapp;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.nicolasrf.wallpaperapp.Adapter.WallpaperListAdapter;
import com.nicolasrf.wallpaperapp.Common.Common;
import com.nicolasrf.wallpaperapp.Model.WallpaperItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RandomListActivity extends AppCompatActivity {
    private static final String TAG = "RandomListActivity";

    RecyclerView recyclerView;

    List<WallpaperItem> wallpaperItemList;
    List<String> mKeys = new ArrayList<>();
    WallpaperListAdapter adapter;

    Boolean isScrolling = false;
    int currentItems, totalItems, scrollOutItems;

    ProgressBar recyclerProgressBar;

    GridLayoutManager gridLayoutManager;

    ImageView noInternetImageView;
    Button retryButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_random_list);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Explorar");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        wallpaperItemList = new ArrayList<>();

        noInternetImageView = findViewById(R.id.no_internet_image);
        retryButton = findViewById(R.id.retry_button);

        //Comeince gone
        noInternetImageView.setVisibility(View.GONE);
        retryButton.setVisibility(View.GONE);

        recyclerProgressBar = findViewById(R.id.recycler_progress_bar);

        recyclerView = findViewById(R.id.wallpaper_list_recycler);
        recyclerView.setVisibility(View.GONE);
        recyclerView.setHasFixedSize(true);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new DividerItemDecoration(this,
                DividerItemDecoration.HORIZONTAL));
        recyclerView.addItemDecoration(new DividerItemDecoration(this,
                DividerItemDecoration.VERTICAL));
        gridLayoutManager = new GridLayoutManager(this,2);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setItemViewCacheSize(20);
        adapter = new WallpaperListAdapter(this,wallpaperItemList);
        recyclerView.setAdapter(adapter);

        //Check internet connection
        ConnectivityManager cm =
                (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        if(isConnected) {

            loadBackgroundList();
            setRandomList();
        }
        else {
            noInternetImageView.setVisibility(View.VISIBLE);
            retryButton.setVisibility(View.VISIBLE);
            recyclerProgressBar.setVisibility(View.GONE);

        }

        //Toast.makeText(getContext(), "You are not connected to internet", Toast.LENGTH_SHORT).show();
        retryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Check internet connection
                ConnectivityManager cm =
                        (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                boolean isConnected = activeNetwork != null &&
                        activeNetwork.isConnectedOrConnecting();
                if(isConnected) {
                    recyclerProgressBar.setVisibility(View.VISIBLE);
                    setRandomList();
                    loadBackgroundList();
                    noInternetImageView.setVisibility(View.GONE);
                    retryButton.setVisibility(View.GONE);
                }
                else {
                    noInternetImageView.setVisibility(View.VISIBLE);
                    retryButton.setVisibility(View.VISIBLE);
                    recyclerProgressBar.setVisibility(View.GONE);
                }
            }
        });

    }

    private void setRandomList() {
        recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
                {
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItems = gridLayoutManager.getChildCount();
                totalItems = gridLayoutManager.getItemCount();
                scrollOutItems = gridLayoutManager.findFirstVisibleItemPosition();

                if(isScrolling && (currentItems + scrollOutItems == totalItems))
                {
                    isScrolling = false;
                    //loadBackgroundList(); //Allow infinite scrolling!
                }
            }
        });
    }

    private void loadBackgroundList() {
        FirebaseDatabase.getInstance().getReference(Common.STR_WALLPAPER)
                .orderByChild("id")
                .addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                        WallpaperItem favoriteItem = dataSnapshot.getValue(WallpaperItem.class);
                        wallpaperItemList.add(favoriteItem); //ADD
                        Common.select_background_key = dataSnapshot.getKey(); //KEY ATTACHED TO COMMON
                        String key = dataSnapshot.getKey();
                        mKeys.add(key);

                        recyclerProgressBar.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);


                        Collections.shuffle(wallpaperItemList); //SET RANDOM!!
                        adapter.notifyDataSetChanged();

                        Log.d(TAG, "onChildAdded: ADDED " + key);
                    }

                    @Override
                    public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                        WallpaperItem favoriteItem = dataSnapshot.getValue(WallpaperItem.class);
                        String key = dataSnapshot.getKey();

                        int index = mKeys.indexOf(key);
                        wallpaperItemList.set(index,favoriteItem);

                        recyclerProgressBar.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);

                        adapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                        //WallpaperItem favoriteItem = dataSnapshot.getValue(WallpaperItem.class);
                        String key = dataSnapshot.getKey();

                        int index = mKeys.indexOf(key);
                        wallpaperItemList.remove(index);

                        recyclerProgressBar.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);

                        adapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            finish(); //Close activity when click back button!
        return super.onOptionsItemSelected(item);
    }

}
