package com.nicolasrf.wallpaperapp.Retrofit;

import com.nicolasrf.wallpaperapp.Model.DataMessage;
import com.nicolasrf.wallpaperapp.Model.MyResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface IFCMService {
    @Headers({
        "Content-type:application/json",
        "Authorization:key=AAAA73MjKyg:APA91bGyfnvm0WBsy1mgkzsmD19y_gciVbI4GsRbWAd9OzDrHutdx9Emt0XAoU94gofkTfkVLkfs_UNc_ZowDq6pcmDqXgYCpVBpPxnboZFXFfr-celikDv2QOYteGwP46HxGR0nDpoX"
    })
    @POST("fcm/send")
    Call<MyResponse> sendNotification(@Body DataMessage body);
}
