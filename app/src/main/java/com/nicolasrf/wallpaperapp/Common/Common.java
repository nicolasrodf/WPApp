package com.nicolasrf.wallpaperapp.Common;

import com.nicolasrf.wallpaperapp.Model.WallpaperItem;
import com.nicolasrf.wallpaperapp.Remote.IComputerVision;
import com.nicolasrf.wallpaperapp.Remote.RetrofitClient;
import com.nicolasrf.wallpaperapp.Retrofit.FCMClient;
import com.nicolasrf.wallpaperapp.Retrofit.IFCMService;

public class Common {
    public static final String STR_CATEGORY_BACKGROUND = "CategoryBackground";
    public static final String STR_WALLPAPER = "Backgrounds";
    public static final int PICK_IMAGE_REQUEST = 1001;
    public static String CATEGORY_SELECTED = "Wallpaper"; //nombre por default para los activities que lo requieren
    public static String CATEGORY_ID_SELECTED;
    public static String COMMUNITY_CATEGORY = "06";

    public static final int RC_SIGN_IN = 9999;

    public static final int PERMISSION_REQUEST_CODE = 1000;

    public static WallpaperItem select_background = new WallpaperItem();

    public static String select_background_key; //wallpaper key from firebase

    private static final String FCM_API = "https://fcm.googleapis.com";

    //API
    public static String BASE_URL = "https://westcentralus.api.cognitive.microsoft.com/vision/v1.0/";
    public static IComputerVision getComputerVisionAPI(){
        return RetrofitClient.getClient(BASE_URL).create(IComputerVision.class);
    }

    public static String getAPIAdultEndPoint(){
        return new StringBuilder(BASE_URL).append("analyze?visualFeatures=Adult&language=en").toString();
    }

    //FCM
    public static IFCMService getFCMService(){
        return FCMClient.getClient(FCM_API).create(IFCMService.class);
    }
}
