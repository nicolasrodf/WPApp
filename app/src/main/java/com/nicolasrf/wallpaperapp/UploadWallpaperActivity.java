package com.nicolasrf.wallpaperapp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.nicolasrf.wallpaperapp.Common.Common;
import com.nicolasrf.wallpaperapp.Helper.GlideApp;
import com.nicolasrf.wallpaperapp.Model.CategoryItem;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;

import dmax.dialog.SpotsDialog;
import id.zelory.compressor.Compressor;

public class UploadWallpaperActivity extends AppCompatActivity {
    private static final String TAG = "UploadWallpaperActivity";

    ImageView previewImageView;
    Button uploadButton,browserButton;
    MaterialSpinner spinner;

    //aMaterial sponner data
    Map<String,String> spinnerData = new HashMap<>();

    private Uri filePath;

    String imageCompressString="";
    String fileName="";

    FirebaseStorage storage;
    StorageReference storageReference;

    AlertDialog dialog;

    private File compressedImageFile;

    String lastKey;

    String categoryIdSelect="";

    //private Date currentDateGregorian;
    String date_created;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_wallpaper);

        //firebase storage
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();

        //create dialog to show after upload to storage and before set image in database
        dialog = new SpotsDialog(UploadWallpaperActivity.this);

        //View
        previewImageView = findViewById(R.id.image_preview);
        browserButton = findViewById(R.id.browser_button);
        uploadButton = findViewById(R.id.set_wallpaper_button);
        spinner = findViewById(R.id.spinner);

        //Load Spinner data
        loadCategoryToSpinner();

        //Button event
        browserButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkPermissions();
            }
        });

        uploadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(spinner.getSelectedIndex() == 0) //Hint, not choose anymore
                {
                    Toast.makeText(UploadWallpaperActivity.this, "Elige una categoría.", Toast.LENGTH_SHORT).show();
                } else {
                    //Check internet connection
                    ConnectivityManager cm =
                            (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                    boolean isConnected = activeNetwork != null &&
                            activeNetwork.isConnectedOrConnecting();
                    if(isConnected) {

                        uploadImage();
                    }
                    else {
                        Toast.makeText(UploadWallpaperActivity.this, "No hay conexión a Internet. Intente nuevamente", Toast.LENGTH_LONG).show();

                    }

                }

            }
        });

        //DATE*****

        int month = getDateGregorian().getMonth() + 1;
        int year = getDateGregorian().getYear() + 1900;
        String minutes = String.valueOf(getDateGregorian().getMinutes());
        if(getDateGregorian().getMinutes() >=0 && getDateGregorian().getMinutes()<=9)
            minutes = new StringBuilder().append("0").append(getDateGregorian().getMinutes()).toString();

        //currentDateGregorian = getDateGregorian();
        date_created = new StringBuilder()
                .append(getDateGregorian().getDate())
                .append("/")
                .append(month)
                .append("/")
                .append(year)
                .append(" ")
                .append(getDateGregorian().getHours())
                .append(":")
                .append(minutes)
                .toString();

        getLastBackgroundKey();

    }

    public static Date getDateGregorian(){
        TimeZone timeZone = TimeZone.getDefault();
        Date dateGregorian = new GregorianCalendar(timeZone).getTime();
        return dateGregorian;
    }

    private void sendRequest() {
        Map<String,Object> data = new HashMap<>();
        data.put("imageUrl", imageCompressString);
        //data.put("imageFull", directUrl);
        data.put("categoryId", categoryIdSelect);
        data.put("userId", FirebaseAuth.getInstance().getCurrentUser().getUid()); //NEW FIELD
        data.put("userName", FirebaseAuth.getInstance().getCurrentUser().getDisplayName()); //NEW FIELD
        data.put("date_created", date_created); //NEW FIELD
        data.put("viewCount", 0); //default
        data.put("targetCount", 0); //default
        data.put("reportedCount", 0); //default
        FirebaseDatabase.getInstance().getReference("Requests")
                .push() //
                .setValue(data)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(UploadWallpaperActivity.this, "La imagen ha sido enviada para su aceptación", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(UploadWallpaperActivity.this, "Sending request error " + e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loadCategoryToSpinner() {
        FirebaseDatabase.getInstance()
                .getReference(Common.STR_CATEGORY_BACKGROUND)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for(DataSnapshot postSnapshot:dataSnapshot.getChildren()){
                            CategoryItem item = postSnapshot.getValue(CategoryItem.class);
                            String key = postSnapshot.getKey();

                            spinnerData.put(key,item.getName());
                        }

                        //Becasuee Material spinner will not receive hint se we need custom hint
                        //Tghis is my tip
                        Object[] valueArray = spinnerData.values().toArray();
                        List<Object> valueList = new ArrayList<>();
                        valueList.add(0,"Categoría"); //We will add first item is Hint
                        valueList.addAll(Arrays.asList(valueArray)); //And add all remain cateogry name
                        spinner.setItems(valueList); //set source data for spinner
                        spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                                //When user choose category, we will get categoryId (key)
                                Object[] keyArray = spinnerData.keySet().toArray();
                                List<Object> keyList = new ArrayList<>();
                                keyList.add(0,"Category_Key");
                                keyList.addAll(Arrays.asList(keyArray));
                                categoryIdSelect = keyList.get(position).toString(); //Assign key when user choose category
                            }
                        });
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    private void deleteFileFromStorage(final String fileName) {
        storageReference.child("images/"+fileName)
                .delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(UploadWallpaperActivity.this, "Your image is adult content and will be deleted", Toast.LENGTH_SHORT).show();
                    }
                });
        storageReference.child("images/"+fileName+"_compressed")
                .delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        finish();
                    }
                });
    }

    public void checkPermissions(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (ContextCompat.checkSelfPermission(UploadWallpaperActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                Toast.makeText(UploadWallpaperActivity.this, "Permiso denegado", Toast.LENGTH_LONG).show();
                ActivityCompat.requestPermissions(UploadWallpaperActivity.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            } else {
                initImageCropper();
            }
        } else {
            initImageCropper();
        }
    }

    private void uploadImage() {

        if(filePath != null){

            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Uploading...");
            progressDialog.show();

            //compress before upload

            //create file
            File file = new File(filePath.getPath());

            try {
                compressedImageFile = new Compressor(UploadWallpaperActivity.this)
                        .setMaxHeight(1920)
                        .setMaxWidth(1080)
                        .setQuality(50)
                        .compressToFile(file);
            } catch (IOException e) {
                e.printStackTrace();
            }

            //create Uri from file (compressed in this case)
            final Uri fileCompressedPath = Uri.fromFile(compressedImageFile);

            fileName = UUID.randomUUID().toString();

            final StorageReference ref = storageReference.child(new StringBuilder("images/").append(fileName).append("_compressed").toString());
            final UploadTask uploadTask  = ref.putFile(fileCompressedPath);
            uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    progressDialog.dismiss();
                    //GET DOWNLOAD URI
                    Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                        @Override
                        public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                            if (!task.isSuccessful()) {
                                throw task.getException();
                            }
                            return ref.getDownloadUrl();

                        }
                    }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                        @Override
                        public void onComplete(@NonNull Task<Uri> task) {
                            if (task.isSuccessful()) {
                                Uri imageCompressUri = task.getResult(); //  URI obtenido desde el urlTask! con el metodo del return anterior!
                                Log.d(TAG, "onComplete: DOWNLOAD URI " + imageCompressUri);
                                imageCompressString = imageCompressUri.toString();
                                //Before create Spot dialog
                                dialog.show();
                                dialog.setMessage("Por favor espere..");
                                sendRequest();
                                //
                                //NOT YET, WILL UPLOAD TO DATABASE IN SERVER APP! ONLY WILL BE SEND TO REQUESTS NODE
                                //saveUrlToDatabase(categoryIdSelect, directUrl, imageCompressString);
                            }
                        }
                    });

                }
            })
            .addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    progressDialog.dismiss();
                    Toast.makeText(UploadWallpaperActivity.this, "ERROR: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            })
            .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    double progress = (100.0*taskSnapshot.getBytesTransferred()/taskSnapshot.getTotalByteCount());
                    progressDialog.setMessage("Uploaded: " + (int)progress+"%");
                }
            });

        }
    }

    private void getLastBackgroundKey(){
        Query query = FirebaseDatabase.getInstance().getReference(Common.STR_WALLPAPER).orderByKey().limitToLast(1);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot child: dataSnapshot.getChildren()) {
                    lastKey = child.getKey();
                    Log.d(TAG, "onDataChange: LAST KEY: " + lastKey);

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                filePath = result.getUri();

                //previewImageView.setImageURI(filePath);
                GlideApp.with(UploadWallpaperActivity.this).load(filePath).fitCenter().into(previewImageView);
                //Picasso.get().load(filePath).fit().into(previewImageView);
                uploadButton.setEnabled(true);
                //uploadImage();

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {

                Exception error = result.getError();
                Toast.makeText(UploadWallpaperActivity.this, "Error en recortar la imagen.", Toast.LENGTH_SHORT).show();
            }
        }

    }

    private void initImageCropper() {
        //Set crop properties.
        //Just accept images cropped with Android device screen aspect ratio
        CropImage.activity()
                .setFixAspectRatio(true)
                .setAspectRatio(9,16)
                .setInitialCropWindowPaddingRatio(0)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMinCropResultSize(1920, 1080)
                .setActivityTitle("RECORTAR")
                .setCropMenuCropButtonTitle("OK")
                .start(this);
    }


    @Override
    public void onBackPressed() {
        deleteFileFromStorage(fileName);
        super.onBackPressed();
    }
}
