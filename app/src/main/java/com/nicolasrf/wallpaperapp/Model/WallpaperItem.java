package com.nicolasrf.wallpaperapp.Model;

import com.google.firebase.database.Exclude;

import java.util.Date;

public class WallpaperItem {

    public long id;
    public String imageUrl;
    String imageThumb;
    public String categoryId;
    public long viewCount;
    public long targetCount;
    public long reportedCount;
    public String userName;
    public String date_created;

    public WallpaperItem() {
    }

    public WallpaperItem(String imageUrl, String categoryId) {
        this.imageUrl = imageUrl;
        this.categoryId = categoryId;
    }

    public WallpaperItem(long id, String imageUrl, String categoryId) {
        this.id = id;
        this.imageUrl = imageUrl;
        this.categoryId = categoryId;
    }

    public WallpaperItem(long id, String imageUrl, String categoryId, long viewCount, long targetCount, long reportedCount, String userName, String date_created) {
        this.id = id;
        this.imageUrl = imageUrl;
        this.categoryId = categoryId;
        this.viewCount = viewCount;
        this.targetCount = targetCount;
        this.reportedCount = reportedCount;
        this.userName = userName;
        this.date_created = date_created;
    }

    public String getImageThumb() {
        return imageThumb;
    }

    public void setImageThumb(String imageThumb) {
        this.imageThumb = imageThumb;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDate_created() {
        return date_created;
    }

    public void setDate_created(String date_created) {
        this.date_created = date_created;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTargetCount() {
        return targetCount;
    }

    public void setTargetCount(long targetCount) {
        this.targetCount = targetCount;
    }

    public long getReportedCount() {
        return reportedCount;
    }

    public void setReportedCount(long reportedCount) {
        this.reportedCount = reportedCount;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public long getViewCount() {
        return viewCount;
    }

    public void setViewCount(long viewCount) {
        this.viewCount = viewCount;
    }
}
