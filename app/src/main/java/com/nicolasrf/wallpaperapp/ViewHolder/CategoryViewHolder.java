package com.nicolasrf.wallpaperapp.ViewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nicolasrf.wallpaperapp.Interface.ItemClickListener;
import com.nicolasrf.wallpaperapp.R;

public class CategoryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public TextView categoryNameTextView;
    public ImageView backgroundImageView;

    ItemClickListener itemClickListener;

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public CategoryViewHolder(View itemView) {
        super(itemView);
        backgroundImageView = itemView.findViewById(R.id.image_view);
        categoryNameTextView = itemView.findViewById(R.id.name_text_view);

        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        itemClickListener.onClick(view,getAdapterPosition());
    }
}
