package com.nicolasrf.wallpaperapp;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.WallpaperManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.Group;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.Sharer;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.nicolasrf.wallpaperapp.Common.Common;
import com.nicolasrf.wallpaperapp.Database.DataSource.RecentRepository;
import com.nicolasrf.wallpaperapp.Database.LocalDatabase.LocalDatabase;
import com.nicolasrf.wallpaperapp.Database.LocalDatabase.RecentsDataSource;
import com.nicolasrf.wallpaperapp.Database.Recents;
import com.nicolasrf.wallpaperapp.Helper.GlideApp;
import com.nicolasrf.wallpaperapp.Helper.SaveImageHelper;
import com.nicolasrf.wallpaperapp.Model.DataMessage;
import com.nicolasrf.wallpaperapp.Model.MyResponse;
import com.nicolasrf.wallpaperapp.Model.Reported;
import com.nicolasrf.wallpaperapp.Model.Token;
import com.nicolasrf.wallpaperapp.Model.WallpaperItem;
import com.nicolasrf.wallpaperapp.Retrofit.IFCMService;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import dmax.dialog.SpotsDialog;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ViewWallpaperActivity extends AppCompatActivity {
    private static final String TAG = "ViewWallpaperActivity";

    ImageView imageView;
    ConstraintLayout rootLayout;

    FirebaseDatabase firebaseDatabase;
    DatabaseReference tokens;

    //RoomDatabase
    CompositeDisposable compositeDisposable;
    RecentRepository recentRepository;

    CallbackManager callbackManager;
    ShareDialog shareDialog;

    CheckBox fabSelectorCheckbox;

    Toolbar toolbar;

    long count;

    Recents recents;

    Group group, group2;

    ArrayAdapter<String> adapter;

    ProgressBar determinateProgress;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Common.PERMISSION_REQUEST_CODE:
            {
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    AlertDialog dialog = new SpotsDialog(ViewWallpaperActivity.this);
                    dialog.setMessage("Please waiting...");

                    String fileName = UUID.randomUUID().toString()+".png";
                    Picasso.get()
                            .load(Common.select_background.getImageUrl())
                            .into(new SaveImageHelper(getBaseContext(),
                                    dialog,
                                    getApplicationContext().getContentResolver(),
                                    fileName,
                                    "NICODev Wallpaper Image"));
                }
                else
                    Toast.makeText(this, "You need accept this permission to download image", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_wallpaper);

        toolbar = findViewById(R.id.toolbar);
        //get category name before set toolbar name
        toolbar.setTitle(Common.CATEGORY_SELECTED);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        group = findViewById(R.id.group);
        group.setVisibility(View.GONE); //Comience Gone

        group2 = findViewById(R.id.group2);
        group2.setVisibility(View.GONE); //Comience Gone

        //Check internet connection
        ConnectivityManager cm =
                (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        if(isConnected) {

            //ALLOW FUNCTIONALITY
            setAllFuncionalities();

        }
        else {
            group2.setVisibility(View.VISIBLE); //
            group.setVisibility(View.GONE);
        }

        Button retryButton = findViewById(R.id.retry_button);
        retryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Check internet connection
                ConnectivityManager cm =
                        (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                boolean isConnected = activeNetwork != null &&
                        activeNetwork.isConnectedOrConnecting();
                if(isConnected) {
                    group.setVisibility(View.VISIBLE);
                    setAllFuncionalities();
                    group2.setVisibility(View.GONE);
                }
                else {
                    group2.setVisibility(View.VISIBLE);
                    group.setVisibility(View.INVISIBLE);
                }
            }
        });

    }

    private void setAllFuncionalities(){

        firebaseDatabase = FirebaseDatabase.getInstance();
        tokens = firebaseDatabase.getReference("tokens");

        Log.d(TAG, "setAllFuncionalities: CATEGORY: " + Common.CATEGORY_SELECTED);

        Log.d(TAG, "onCreate: SELECT BACKGROUND ID " + Common.select_background.getId());

        //setBitmapDialog = new SpotsDialog(ViewWallpaperActivity.this);

        determinateProgress = findViewById(R.id.determinate_progress_bar);
        determinateProgress.setVisibility(View.GONE);

        fabSelectorCheckbox = findViewById(R.id.favorite_checkbox);

        //Favorites selector functionallity
        fabSelectorCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(!isChecked){
                    Log.d(TAG, "onCheckedChanged: " + "not checked ");
                    //delete
                    DatabaseReference dbFavs = FirebaseDatabase.getInstance().getReference("users")
                            .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                            .child("favorites")
                            .child(Common.select_background_key);
                    dbFavs.removeValue()
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    Toast.makeText(ViewWallpaperActivity.this, "Removido de Favoritos", Toast.LENGTH_SHORT).show();
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(ViewWallpaperActivity.this, "Error " + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
                    compoundButton.setButtonDrawable(R.drawable.ic_favorite_default);

                } else {
                    Log.d(TAG, "onCheckedChanged: " + "is checked " + Common.CATEGORY_SELECTED);
                    //set to database
                    DatabaseReference dbFavs = FirebaseDatabase.getInstance().getReference("users")
                            .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                            .child("favorites")
                            .child(Common.select_background_key);
                    dbFavs.setValue(Common.select_background)
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    Toast.makeText(ViewWallpaperActivity.this, "Guardado en Favoritos", Toast.LENGTH_SHORT).show();
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(ViewWallpaperActivity.this, "Error " + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
                    //set color
                    compoundButton.setButtonDrawable(R.drawable.ic_favorite_active);
                }
            }
        });

        //Init Facebook
        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);

        rootLayout = findViewById(R.id.root_layout);

        //Init Roomtabase
        compositeDisposable = new CompositeDisposable();
        LocalDatabase database = LocalDatabase.getInstance(this);
        recentRepository = RecentRepository.getInstance(RecentsDataSource.getInstance(database.recentsDAO()));


        //****Set to Main, Lock, Both, or Download
        final FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Dialog dialog = new Dialog(ViewWallpaperActivity.this);
                dialog.setContentView(R.layout.custom_dialog);
                dialog.setTitle("Title...");
                final ListView listView = dialog.findViewById(R.id.list);
                // Defined Array values to show in ListView
                String[] values = new String[] { "Fondo en la pantalla principal",
                        "Descargar a mi celular",
                };

                String[] valuesNougat = new String[] { "Fondo en la pantalla principal",
                        "Fondo en la pantalla de bloqueo",
                        "Fondo en la pantalla principal y de bloqueo",
                        "Descargar a mi celular",
                };

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                    adapter = new ArrayAdapter<String>(ViewWallpaperActivity.this, android.R.layout.simple_list_item_1, valuesNougat);
                else
                    adapter = new ArrayAdapter<String>(ViewWallpaperActivity.this, android.R.layout.simple_list_item_1, values);
                //ArrayAdapter<String> adapter = new ArrayAdapter<String>(ViewWallpaperActivity.this, android.R.layout.simple_list_item_1, values);


                // Assign adapter to ListView
                listView.setAdapter(adapter);

                dialog.show();

                // Get ListView object from xml

                // ListView Item Click Listener
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {

                        // ListView Clicked item index
                        int itemPosition     = position;

                        // ListView Clicked item value
                        String  itemValue    = (String) listView.getItemAtPosition(position);

                        // This starts the AsyncTask
                        // Doesn't need to be in onCreate()
                        //new MyTask().execute(itemValue);

                        determinateProgress.setVisibility(View.VISIBLE);
//                        setBitmapDialog.show();
//                        setBitmapDialog.setMessage("Please waiting...");

                        switch (itemValue) {
                            case "Fondo en la pantalla principal":

                                Glide.with(ViewWallpaperActivity.this)
                                        .asBitmap()
                                        .load(Common.select_background.getImageUrl())
                                        .into(new SimpleTarget<Bitmap>() {
                                            @Override
                                            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                                dialog.dismiss();
                                                //Dont needed. When is centerCrop() image sets correct height and width!
                                                //get height and width of screen device
                                                DisplayMetrics displayMetrics = new DisplayMetrics();
                                                getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                                                int height = displayMetrics.heightPixels;
                                                int width = displayMetrics.widthPixels;

                                                Bitmap bitmap = Bitmap.createScaledBitmap(resource, width, height, true);

                                                WallpaperManager wallpaperManager = WallpaperManager.getInstance(ViewWallpaperActivity.this);

                                                //set to resource bitmap
//                                            resource.setHeight(height);
//                                            resource.setWidth(width);
                                                //set wallpaper using resoruce bitmap
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                                    try {
                                                        //wallpaperManager.setBitmap(resource);
                                                        wallpaperManager.setBitmap(resource, null, true,
                                                                WallpaperManager.FLAG_SYSTEM);
                                                    } catch (IOException e) {
                                                        e.printStackTrace();
                                                    }
                                                } else {
                                                    try {
                                                        wallpaperManager.setWallpaperOffsetSteps(1, 1);
                                                        wallpaperManager.suggestDesiredDimensions(width, height);
                                                        wallpaperManager.setBitmap(bitmap);
                                                    } catch (IOException e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                                determinateProgress.setVisibility(View.GONE);
                                                group.setVisibility(View.GONE);
                                                //setBitmapDialog.dismiss();
                                                Snackbar.make(rootLayout, "Fondo configurado correctamente a pantalla principal", Snackbar.LENGTH_SHORT).show();
                                                increaseCount("targetCount");
                                            }
                                        });
                                break;

                            case "Fondo en la pantalla de bloqueo":
                                Glide.with(ViewWallpaperActivity.this)
                                        .asBitmap()
                                        .load(Common.select_background.getImageUrl())
                                        .into(new SimpleTarget<Bitmap>() {
                                            @RequiresApi(api = Build.VERSION_CODES.N)
                                            @Override
                                            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                                dialog.dismiss();
                                                //Dont needed. When is centerCrop() image sets correct height and width!
                                                //get height and width of screen device
                                                DisplayMetrics displayMetrics = new DisplayMetrics();
                                                getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                                                int height = displayMetrics.heightPixels;
                                                int width = displayMetrics.widthPixels;

                                                Bitmap bitmap = Bitmap.createScaledBitmap(resource, width, height, true);
                                                WallpaperManager wallpaperManager = WallpaperManager.getInstance(ViewWallpaperActivity.this);
                                                //set wallpaper using resoruce bitmap
                                                try {
                                                    wallpaperManager.setBitmap(bitmap, null, true,
                                                            WallpaperManager.FLAG_LOCK);
                                                } catch (IOException e) {
                                                    e.printStackTrace();
                                                }
                                                determinateProgress.setVisibility(View.GONE);
                                                group.setVisibility(View.GONE);
                                                Snackbar.make(rootLayout, "Fondo configurado correctamente a pantalla de bloqueo", Snackbar.LENGTH_SHORT).show();
                                                increaseCount("targetCount");
                                            }
                                        });
                                break;

                            case "Fondo en la pantalla principal y de bloqueo":
                                Glide.with(ViewWallpaperActivity.this)
                                        .asBitmap()
                                        .load(Common.select_background.getImageUrl())
                                        .into(new SimpleTarget<Bitmap>() {
                                            @RequiresApi(api = Build.VERSION_CODES.N)
                                            @Override
                                            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                                dialog.dismiss();

                                                //Dont needed. When is centerCrop() image sets correct height and width!
                                                //get height and width of screen device
                                                DisplayMetrics displayMetrics = new DisplayMetrics();
                                                getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                                                int height = displayMetrics.heightPixels;
                                                int width = displayMetrics.widthPixels;

                                                Bitmap bitmap = Bitmap.createScaledBitmap(resource, width, height, true);
                                                WallpaperManager wallpaperManager = WallpaperManager.getInstance(ViewWallpaperActivity.this);
                                                //set wallpaper using resoruce bitmap
                                                try {
//                                                    WallpaperManager.getInstance(getApplicationContext())
//                                                            .setBitmap(resource);
                                                    wallpaperManager.setBitmap(resource, null, true,
                                                            WallpaperManager.FLAG_SYSTEM);
                                                    wallpaperManager.setBitmap(bitmap, null, true,
                                                            WallpaperManager.FLAG_LOCK);
                                                } catch (IOException e) {
                                                    e.printStackTrace();
                                                }
                                                determinateProgress.setVisibility(View.GONE);
                                                group.setVisibility(View.GONE);
                                                Snackbar.make(rootLayout, "Fondo configurado correctamente", Snackbar.LENGTH_SHORT).show();
                                                increaseCount("targetCount");
                                            }
                                        });
                                break;
                            case "Descargar a mi celular":
                                dialog.dismiss();

                                Glide.with(ViewWallpaperActivity.this)
                                        .asBitmap()
                                        .load(Common.select_background.getImageUrl())
                                        .into(new SimpleTarget<Bitmap>() {
                                            @Override
                                            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                                saveImage(resource);
                                                determinateProgress.setVisibility(View.GONE);
                                            }
                                        });
                                break;
                        }
                    }

                });

            }
        });



        //Load image
        imageView = findViewById(R.id.thumb_image_view);
        final ProgressBar imageProgressBar = findViewById(R.id.image_progress_bar);

        GlideApp.with(this)
                .load(Common.select_background.getImageUrl())
                .thumbnail(0.1f)
                .centerCrop()
                .placeholder(R.drawable.placeholder_mod)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, com.bumptech.glide.request.target.Target<Drawable> target, boolean isFirstResource) {
                        Log.d(TAG, "onLoadFailed: GLIDE EXCEPTION " + e.getMessage());
                        imageProgressBar.setVisibility(View.GONE);
                        Log.d(TAG, "onError: CAUSE: " + e.getMessage());
                        if(Common.select_background_key!=null) {
                            Log.d(TAG, "onLoadFailed: BACKGROUND KEY: " + Common.select_background_key);
//                            removeFromFavorites(); //remove if exitsts
//                            removeFromMyUploads(); //remove if exists
//                            removeFromBackgrounds(); //remove if exists
                        }
                        //clear repository
                        recentRepository.deleteRecents(recents);

                        finish();
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, com.bumptech.glide.request.target.Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        imageProgressBar.setVisibility(View.GONE);

                        //Al cargar la image recien hacer esto para evitar que ponga un nuevo nodo con el view count
                        //Checkear para evitar error que al iniciar app no almacena el key cuando se va directo al Recents fragment
                        if(Common.select_background_key!=null) {
                            //Check if is in favorite list
                            FirebaseDatabase.getInstance().getReference("users")
                                    .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                    .child("favorites")
                                    .child(Common.select_background_key)
                                    .addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                            if (dataSnapshot.exists()) {
                                                fabSelectorCheckbox.setButtonDrawable(R.drawable.ic_favorite_active);
                                                fabSelectorCheckbox.setChecked(true);
                                            } else {
                                                fabSelectorCheckbox.setButtonDrawable(R.drawable.ic_favorite_default);
                                                fabSelectorCheckbox.setChecked(false);
                                            }
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                        }
                                    });

                            increaseCount("viewCount");
                        }
                        return false;
                    }

                })
                .into(imageView);


        //add to recents
        addToRecents();

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int visibility = group.getVisibility();

                if(visibility == View.VISIBLE){
                    group.setVisibility(View.GONE);
                } else if (visibility == View.GONE)
                {
                    group.setVisibility(View.VISIBLE);
                }
            }
        });
    }


    private void removeFromFavorites() {
        FirebaseDatabase.getInstance().getReference("users")
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child("favorites")
                .child(Common.select_background_key)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if(dataSnapshot.exists()){
                            dataSnapshot.getRef().removeValue();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
        //dbFavs.removeValue();
    }

    private void removeFromMyUploads() {
        FirebaseDatabase.getInstance().getReference("users")
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child("uploads")
                .child(Common.select_background_key)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if(dataSnapshot.exists()){
                            dataSnapshot.getRef().removeValue();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
        //dbFavs.removeValue();
    }

    private void removeFromBackgrounds() {
        FirebaseDatabase.getInstance().getReference(Common.STR_WALLPAPER)
                .child(Common.select_background_key)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if(dataSnapshot.exists()){
                            dataSnapshot.getRef().removeValue();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
        //dbFavs.removeValue();
    }

    private void showReportedDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("REPORTAR");

        LayoutInflater inflater = LayoutInflater.from(this);
        View layout_settings = inflater.inflate(R.layout.report_options_layout,null);

        final RadioButton inapropiateRadio= layout_settings.findViewById(R.id.inapropiate_content_radio);
        final RadioButton lowResolutionRadio = layout_settings.findViewById(R.id.low_resolution_button);
        final RadioButton contentDifferentRadio = layout_settings.findViewById(R.id.content_is_different_radio);

        alertDialog.setView(layout_settings);
        alertDialog.setPositiveButton("ENVIAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                //increase reported count
                increaseCount("reportedCount");

                //create Reported in database

                final Reported reported = new Reported();
//                reported.setWallpaperId(Common.select_background_key);
//                reported.setReason(inapropiateRadio.getText().toString());
                reported.setCategoryId(Common.CATEGORY_ID_SELECTED);
                reported.setImageUrl(Common.select_background.getImageUrl());

                if(inapropiateRadio.isChecked()) {
                    reported.setWallpaperId(Common.select_background_key);
                    reported.setReason(inapropiateRadio.getText().toString());
                }
                else if(lowResolutionRadio.isChecked()) {
                    reported.setWallpaperId(Common.select_background_key);
                    reported.setReason(lowResolutionRadio.getText().toString());
                }
                else if(contentDifferentRadio.isChecked()) {
                    reported.setWallpaperId(Common.select_background_key);
                    reported.setReason(contentDifferentRadio.getText().toString());
                }

                FirebaseDatabase.getInstance().getReference("Reported")
                        .child(Common.select_background_key)
                        .setValue(reported)
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Toast.makeText(ViewWallpaperActivity.this, "Your reported has been sent.", Toast.LENGTH_SHORT).show();
                                sendNotificationToServer(FirebaseAuth.getInstance().getCurrentUser().getUid());

                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(ViewWallpaperActivity.this, "Error sending report", Toast.LENGTH_SHORT).show();
                            }
                        });

                dialogInterface.dismiss();

            }
        }).setNegativeButton("CANCELAR",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        alertDialog.show();
    }

    private void sendNotificationToServer(final String userId) {
        //Get Server token (server_app_01 is phone put by us by Server app)

        //Get token from FirebaseDtatabase!!

        tokens.child("server_app_01")
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if(dataSnapshot.exists()){
                            Token token = dataSnapshot.getValue(Token.class);
                            String userToken = token.getToken();

                            //
                            Map<String,String> contentSend = new HashMap<>();
                            contentSend.put("title","NICODev");
                            contentSend.put("message", "Tienes un nuevo reporte de : " + userId);
                            DataMessage dataMessage = new DataMessage();
                            //Toast.makeText(CartActivity.this, "TOEKN; " + response.body().getToken(), Toast.LENGTH_SHORT).show();
                            dataMessage.setTo(userToken);
                            dataMessage.setData(contentSend);

                            IFCMService ifcmService = Common.getFCMService();
                            ifcmService.sendNotification(dataMessage)
                                    .enqueue(new Callback<MyResponse>() {
                                        @Override
                                        public void onResponse(Call<MyResponse> call, Response<MyResponse> response) {
                                            if(response.code() == 200){
                                                if(response.body().success == 1){
                                                    Toast.makeText(ViewWallpaperActivity.this, "Gracias, Reporte Enviado!", Toast.LENGTH_SHORT).show();
                                                    finish();
                                                } else {
                                                    Toast.makeText(ViewWallpaperActivity.this, "Error en envio de reporte", Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<MyResponse> call, Throwable t) {
                                            Toast.makeText(ViewWallpaperActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                                        }
                                    });
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });


    }

    private void increaseCount(final String propertieName) {
        FirebaseDatabase.getInstance().getReference(Common.STR_WALLPAPER)
                .child(Common.select_background_key)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        //sOLO si existe el background para evitar q en Recents setee uno nuevo al cambiar algunas propiuedades,
                        if(dataSnapshot.exists()) {
                            if (dataSnapshot.hasChild(propertieName)) {
                                WallpaperItem wallpaperItem = dataSnapshot.getValue(WallpaperItem.class);
                                switch (propertieName) {
                                    case ("viewCount"):
                                        count = wallpaperItem.getViewCount() + 1;
                                        break;
                                    case ("reportedCount"):
                                        count = wallpaperItem.getReportedCount() + 1;
                                        break;
                                    case ("targetCount"):
                                        count = wallpaperItem.getTargetCount() + 1;
                                        break;
                                }
                                //Update
                                Map<String, Object> update_view = new HashMap<>();
                                update_view.put(propertieName, count);
                                FirebaseDatabase.getInstance().getReference(Common.STR_WALLPAPER)
                                        .child(Common.select_background_key)
                                        .updateChildren(update_view)
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {

                                            }
                                        }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Toast.makeText(ViewWallpaperActivity.this, "Cannot update count", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            } else { //set default if view count is not set
                                Map<String, Object> update_view = new HashMap<>();
                                update_view.put(propertieName, Long.valueOf(1));
                                FirebaseDatabase.getInstance().getReference(Common.STR_WALLPAPER)
                                        .child(Common.select_background_key)
                                        .updateChildren(update_view)
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {

                                            }
                                        }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Toast.makeText(ViewWallpaperActivity.this, "Cannot update count", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    private void addToRecents() {
        Log.d(TAG, "addToRecents: STARTED");
        Disposable disposable = Observable.create(new ObservableOnSubscribe<Object>() {
            @Override
            public void subscribe(ObservableEmitter<Object> emitter) throws Exception {
                recents = new Recents(Common.select_background.getImageUrl(),
                        Common.select_background.getCategoryId(),
                        Common.select_background.getViewCount(),
                        Common.select_background.getTargetCount(),
                        Common.select_background.getReportedCount(),
                        String.valueOf(System.currentTimeMillis()),
                        Common.select_background_key);
                Log.d(TAG, "subscribe: RECENTS ADDED " +recents.getImageUrl());
                recentRepository.insertRecents(recents);
                emitter.onComplete();
            }
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<Object>() {
                    @Override
                    public void accept(Object o) throws Exception {
                        Toast.makeText(ViewWallpaperActivity.this, "added?", Toast.LENGTH_SHORT).show();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Log.d(TAG, "accept: " + throwable.getMessage());
                    }
                }, new Action() {
                    @Override
                    public void run() throws Exception {

                    }
                });

        compositeDisposable.add(disposable);

        //despues de agregar a Recents recien puedo setear el texto al toolbar
        //toolbar.setTitle(Common.CATEGORY_SELECTED);

    }

    @Override
    protected void onDestroy() {
        //get().cancelRequest(target);
        compositeDisposable.clear();
        super.onDestroy();
    }

    //****App Toolbar****

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.wallpaper, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if(id == android.R.id.home)
            finish(); //Close activity when click back button!

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_report) {
            //Toast.makeText(this, "implement late", Toast.LENGTH_SHORT).show();
            showReportedDialog();
            return true;
        } else  if (id == R.id.action_info) {
            //Toast.makeText(this, "implement late", Toast.LENGTH_SHORT).show();
            showWallpaperInfo();
            return true;
        } else  if (id == R.id.action_share) {
            //Toast.makeText(this, "implement late", Toast.LENGTH_SHORT).show();
            shareToFacebook();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void shareToFacebook() {

        //Create callback
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                Toast.makeText(ViewWallpaperActivity.this, "Share successfully", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancel() {
                Toast.makeText(ViewWallpaperActivity.this, "Share cancelled", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(ViewWallpaperActivity.this, "Error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        //We will fetch photo from link an converti to bitmap
        Glide.with(ViewWallpaperActivity.this)
                .asBitmap()
                .load(Common.select_background.getImageUrl())
                .into(new SimpleTarget<Bitmap>() {
                          @Override
                          public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                              SharePhoto sharePhoto = new SharePhoto.Builder()
                                      .setBitmap(resource)
                                      .build();
                              if(shareDialog.canShow(SharePhotoContent.class)){
                                  SharePhotoContent content = new SharePhotoContent.Builder()
                                          .addPhoto(sharePhoto)
                                          .build();
                                  shareDialog.show(content);
                              }
                          }
                      });

    }

    private void showWallpaperInfo() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("INFO");

        LayoutInflater inflater = LayoutInflater.from(this);
        View layout_settings = inflater.inflate(R.layout.info_card_layout,null);

        final TextView uploaderTextView = layout_settings.findViewById(R.id.uploader_text_view);
        final TextView dateTextView = layout_settings.findViewById(R.id.date_text_view);

        //get info and set to layout
        FirebaseDatabase.getInstance().getReference("Backgrounds")
                .child(Common.select_background_key)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        WallpaperItem wallpaperItem = dataSnapshot.getValue(WallpaperItem.class);
                        uploaderTextView.setText(new StringBuilder("Credits: ").append(wallpaperItem.getUserName()));
                        dateTextView.setText(wallpaperItem.getDate_created());
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });


        alertDialog.setView(layout_settings);
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {

                dialogInterface.dismiss();

            }
        });

        alertDialog.show();
    }

    //******SAVE IMAGE METHODS******************

    private String saveImage(Bitmap image) {
        String savedImagePath = null;

        String imageFileName = Common.select_background.getUserName()+"_"+Common.select_background.getDate_created() + ".jpg";
        File storageDir = new File(            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
                + "/WallpaperApp");
        boolean success = true;
        if (!storageDir.exists()) {
            success = storageDir.mkdirs();
        }
        if (success) {
            File imageFile = new File(storageDir, imageFileName);
            savedImagePath = imageFile.getAbsolutePath();
            try {
                OutputStream fOut = new FileOutputStream(imageFile);
                image.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
                fOut.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            // Add the image to the system gallery
            galleryAddPic(savedImagePath);
            Toast.makeText(ViewWallpaperActivity.this, "IMAGE SAVED", Toast.LENGTH_LONG).show();
        }
        return savedImagePath;
    }

    private void galleryAddPic(String imagePath) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(imagePath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        sendBroadcast(mediaScanIntent);
    }


}
