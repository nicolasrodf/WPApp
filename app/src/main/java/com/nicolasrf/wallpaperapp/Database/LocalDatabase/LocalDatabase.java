package com.nicolasrf.wallpaperapp.Database.LocalDatabase;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.nicolasrf.wallpaperapp.Database.Recents;

import static com.nicolasrf.wallpaperapp.Database.LocalDatabase.LocalDatabase.DATABASE_VERSION;

@Database(entities = Recents.class, version = DATABASE_VERSION)
public abstract class LocalDatabase extends RoomDatabase {

    public static final int DATABASE_VERSION = 7;
    public static final String DATABASE_NAME = "NicoWallpaper";

    public abstract RecentsDAO recentsDAO();

    public static LocalDatabase instance;

    public static LocalDatabase getInstance(Context context){
        if(instance == null){
            instance = Room.databaseBuilder(context,LocalDatabase.class,DATABASE_NAME)
                    .fallbackToDestructiveMigration()
                    .allowMainThreadQueries()
                    .build();
        }

        return instance;
    }
}
