package com.nicolasrf.wallpaperapp.Database;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.support.annotation.NonNull;

@Entity(tableName = "recents",primaryKeys = {"imageUrl","categoryId"})
public class Recents {

    @ColumnInfo(name = "imageUrl")
    @NonNull
    private String imageUrl;

    @ColumnInfo(name = "categoryId")
    @NonNull
    private String categoryId;

    @ColumnInfo(name = "viewCount")
    @NonNull
    private long viewCount;

    @ColumnInfo(name = "targetCount")
    @NonNull
    private long targetCount;

    @ColumnInfo(name = "reportedCount")
    @NonNull
    private long reportedCount;

    @ColumnInfo(name = "saveTime")
    @NonNull
    private String saveTime;

    //firebase key object
    @ColumnInfo(name = "key")
    @NonNull
    private String key;

    public Recents() {
    }

    public Recents(@NonNull String imageUrl, @NonNull String categoryId,
                   @NonNull long viewCount, @NonNull long targetCount, @NonNull long reportedCount, @NonNull String saveTime, @NonNull String key) {
        this.imageUrl = imageUrl;
        this.categoryId = categoryId;
        this.viewCount = viewCount;
        this.targetCount = targetCount;
        this.reportedCount = reportedCount;
        this.saveTime = saveTime;
        this.key = key;
    }

    @NonNull
    public long getTargetCount() {
        return targetCount;
    }

    public void setTargetCount(@NonNull long targetCount) {
        this.targetCount = targetCount;
    }

    @NonNull
    public long getReportedCount() {
        return reportedCount;
    }

    public void setReportedCount(@NonNull long reportedCount) {
        this.reportedCount = reportedCount;
    }

    @NonNull
    public long getViewCount() {
        return viewCount;
    }

    public void setViewCount(@NonNull long viewCount) {
        this.viewCount = viewCount;
    }

    @NonNull
    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(@NonNull String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @NonNull
    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(@NonNull String categoryId) {
        this.categoryId = categoryId;
    }

    @NonNull
    public String getSaveTime() {
        return saveTime;
    }

    public void setSaveTime(@NonNull String saveTime) {
        this.saveTime = saveTime;
    }

    @NonNull
    public String getKey() {
        return key;
    }

    public void setKey(@NonNull String key) {
        this.key = key;
    }
}

