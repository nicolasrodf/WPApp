package com.nicolasrf.wallpaperapp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.nicolasrf.wallpaperapp.Common.Common;
import com.nicolasrf.wallpaperapp.Helper.GlideApp;
import com.nicolasrf.wallpaperapp.Model.WallpaperItem;

public class ViewMyUploadActivity extends AppCompatActivity {
    private static final String TAG = "ViewMyUploadActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_my_upload);

        final ProgressBar progressBar = findViewById(R.id.image_progress_bar);

        ImageView imageView = findViewById(R.id.imageView);

        GlideApp.with(this)
                .load(Common.select_background.getImageUrl())
                .thumbnail(0.1f)
                .placeholder(R.drawable.placeholder)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(imageView);

        ImageView infoImageView = findViewById(R.id.info_image_view);
        infoImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setDialog();
            }
        });

        ImageView deleteImageView = findViewById(R.id.delete_image_view);
        deleteImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDeleteDialog();
            }
        });

    }

    private void showDeleteDialog() {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("BORRAR");
        alertDialog.setMessage("Seguro que desea eliminar la imagen?");
        alertDialog.setPositiveButton("SI", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //delete from Backgrounds
                FirebaseDatabase.getInstance().getReference("Backgrounds")
                        .child(Common.select_background_key)
                        .removeValue()
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Log.d(TAG, "onSuccess: DELETED FROM BACKGROUNDS");
                                finish();
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(ViewMyUploadActivity.this, "Error deleting image: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                                Log.d(TAG, "onFailure: ERROR: " + e.getMessage());
                                finish();
                            }
                        });
                //Delete from my Uploads
                FirebaseDatabase.getInstance().getReference("users")
                        .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                        .child("uploads")
                        .child(Common.select_background_key)
                        .removeValue()
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Log.d(TAG, "onSuccess: DELETED FROM uploads");
                                Toast.makeText(ViewMyUploadActivity.this, "Imagen eliminada.", Toast.LENGTH_SHORT).show();
                                finish();
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(ViewMyUploadActivity.this, "Error deleting image: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                                Log.d(TAG, "onFailure: ERROR: " + e.getMessage());
                                finish();
                            }
                        });

                //Delete from Storage
                // Create a storage reference from our app
                FirebaseStorage storage = FirebaseStorage.getInstance();
                //StorageReference storageRefFull = storage.getReferenceFromUrl(Common.request_selected.getImageFull());
                StorageReference storageRefUrl = storage.getReferenceFromUrl(Common.select_background.getImageUrl());
                // Delete the file url
                storageRefUrl.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        // File deleted successfully
                        Log.d(TAG, "onSuccess: DELETED IMAGE URL FROM STORAGE");
                        finish(); //finish activity
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Uh-oh, an error occurred!
                        Log.d(TAG, "onFailure: ERROR: " + exception.getMessage());
                        finish(); //finish activity
                    }
                });
            }
        }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        alertDialog.show();
    }

    private void setDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("INFO");

        LayoutInflater inflater = LayoutInflater.from(this);
        View layout_settings = inflater.inflate(R.layout.info_card_layout,null);

        final TextView viewsTextView = layout_settings.findViewById(R.id.uploader_text_view);
        final TextView targetsTextView = layout_settings.findViewById(R.id.date_text_view);

        //get info and set to layout
        FirebaseDatabase.getInstance().getReference("Backgrounds")
                .child(Common.select_background_key)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        WallpaperItem wallpaperItem = dataSnapshot.getValue(WallpaperItem.class);
//                        if(wallpaperItem.getViewCount()){
//
//                        }
                        viewsTextView.setText(new StringBuilder("Vistas: ").append(wallpaperItem.getViewCount()));
                        targetsTextView.setText(new StringBuilder("Descargas: ").append(wallpaperItem.getTargetCount()));
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });


        alertDialog.setView(layout_settings);
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {

                dialogInterface.dismiss();

            }
        });

        alertDialog.show();
    }
}
