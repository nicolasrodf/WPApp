package com.nicolasrf.wallpaperapp;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.WallpaperManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.nicolasrf.wallpaperapp.Common.Common;
import com.nicolasrf.wallpaperapp.Helper.GlideApp;
import com.nicolasrf.wallpaperapp.Model.CategoryItem;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;

import dmax.dialog.SpotsDialog;
import id.zelory.compressor.Compressor;

public class SetFromGalleryActivity extends AppCompatActivity {
    private static final String TAG = "SetFromGalleryActivity";

    ImageView previewImageView;
    Button setWallpaperButton,browserButton;

    private Uri filePath;

    FirebaseStorage storage;
    StorageReference storageReference;

    AlertDialog dialog;

    ArrayAdapter<String> adapter;

    ProgressBar determinateProgress;

    RelativeLayout rootLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_from_gallery);

        //firebase storage
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();

        //create dialog to show after upload to storage and before set image in database
        dialog = new SpotsDialog(SetFromGalleryActivity.this);

        determinateProgress = findViewById(R.id.determinate_progress_bar);
        determinateProgress.setVisibility(View.GONE);

        //View
        previewImageView = findViewById(R.id.image_preview);
        browserButton = findViewById(R.id.browser_button);
        setWallpaperButton = findViewById(R.id.set_wallpaper_button);
        rootLayout = findViewById(R.id.root_layout);

        //Button event
        browserButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkPermissions();
            }
        });

        setWallpaperButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final Dialog dialog = new Dialog(SetFromGalleryActivity.this);
                dialog.setContentView(R.layout.custom_dialog);
                final ListView listView = dialog.findViewById(R.id.list);
                // Defined Array values to show in ListView
                String[] values = new String[]{"Fondo de pantalla principal"
                };

                String[] valuesNougat = new String[]{"Fondo de pantalla principal",
                        "Fondo de pantalla de bloqueo",
                        "Fondo de pantalla principal/bloqueo"
                };

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                    adapter = new ArrayAdapter<String>(SetFromGalleryActivity.this, android.R.layout.simple_list_item_1, valuesNougat);
                else
                    adapter = new ArrayAdapter<String>(SetFromGalleryActivity.this, android.R.layout.simple_list_item_1, values);
                //ArrayAdapter<String> adapter = new ArrayAdapter<String>(ViewWallpaperActivity.this, android.R.layout.simple_list_item_1, values);


                // Assign adapter to ListView
                listView.setAdapter(adapter);

                dialog.show();

                // Get ListView object from xml

                // ListView Item Click Listener
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {

                        // ListView Clicked item index
                        int itemPosition = position;

                        // ListView Clicked item value
                        String itemValue = (String) listView.getItemAtPosition(position);

                        determinateProgress.setVisibility(View.VISIBLE);

                        browserButton.setVisibility(View.GONE);
                        setWallpaperButton.setVisibility(View.GONE);

                        switch (itemValue) {
                            case "Fondo de pantalla principal":

                                Glide.with(SetFromGalleryActivity.this)
                                        .asBitmap()
                                        .load(filePath)
                                        .into(new SimpleTarget<Bitmap>() {
                                            @Override
                                            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                                dialog.dismiss();
                                                //Dont needed. When is centerCrop() image sets correct height and width!
                                                //get height and width of screen device
                                                DisplayMetrics displayMetrics = new DisplayMetrics();
                                                getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                                                int height = displayMetrics.heightPixels;
                                                int width = displayMetrics.widthPixels;

                                                Bitmap bitmap = Bitmap.createScaledBitmap(resource, width, height, true);

                                                WallpaperManager wallpaperManager = WallpaperManager.getInstance(SetFromGalleryActivity.this);

                                                //set to resource bitmap
//                                            resource.setHeight(height);
//                                            resource.setWidth(width);
                                                //set wallpaper using resoruce bitmap
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                                    try {
                                                        //wallpaperManager.setBitmap(resource);
                                                        wallpaperManager.setBitmap(resource, null, true,
                                                                WallpaperManager.FLAG_SYSTEM);
                                                    } catch (IOException e) {
                                                        e.printStackTrace();
                                                    }
                                                } else {
                                                    try {
                                                        wallpaperManager.setWallpaperOffsetSteps(1, 1);
                                                        wallpaperManager.suggestDesiredDimensions(width, height);
                                                        wallpaperManager.setBitmap(bitmap);
                                                    } catch (IOException e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                                determinateProgress.setVisibility(View.GONE);
                                                //setBitmapDialog.dismiss();
                                                Snackbar.make(rootLayout, "Wallpaper was set to main screen", Snackbar.LENGTH_SHORT).show();
                                            }
                                        });
                                break;

                            case "Fondo de pantalla de bloqueo":
                                Glide.with(SetFromGalleryActivity.this)
                                        .asBitmap()
                                        .load(filePath)
                                        .into(new SimpleTarget<Bitmap>() {
                                            @RequiresApi(api = Build.VERSION_CODES.N)
                                            @Override
                                            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                                dialog.dismiss();
                                                //Dont needed. When is centerCrop() image sets correct height and width!
                                                //get height and width of screen device
                                                DisplayMetrics displayMetrics = new DisplayMetrics();
                                                getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                                                int height = displayMetrics.heightPixels;
                                                int width = displayMetrics.widthPixels;

                                                Bitmap bitmap = Bitmap.createScaledBitmap(resource, width, height, true);
                                                WallpaperManager wallpaperManager = WallpaperManager.getInstance(SetFromGalleryActivity.this);
                                                //set wallpaper using resoruce bitmap
                                                try {
                                                    wallpaperManager.setBitmap(bitmap, null, true,
                                                            WallpaperManager.FLAG_LOCK);
                                                } catch (IOException e) {
                                                    e.printStackTrace();
                                                }
                                                determinateProgress.setVisibility(View.GONE);
                                                Snackbar.make(rootLayout, "Wallpaper was set to lock screen", Snackbar.LENGTH_SHORT).show();
                                            }
                                        });
                                break;

                            case "Fondo de pantalla principal/bloqueo":
                                Glide.with(SetFromGalleryActivity.this)
                                        .asBitmap()
                                        .load(filePath)
                                        .into(new SimpleTarget<Bitmap>() {
                                            @RequiresApi(api = Build.VERSION_CODES.N)
                                            @Override
                                            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                                dialog.dismiss();

                                                //Dont needed. When is centerCrop() image sets correct height and width!
                                                //get height and width of screen device
                                                DisplayMetrics displayMetrics = new DisplayMetrics();
                                                getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                                                int height = displayMetrics.heightPixels;
                                                int width = displayMetrics.widthPixels;

                                                Bitmap bitmap = Bitmap.createScaledBitmap(resource, width, height, true);
                                                WallpaperManager wallpaperManager = WallpaperManager.getInstance(SetFromGalleryActivity.this);
                                                //set wallpaper using resoruce bitmap
                                                try {
//                                                    WallpaperManager.getInstance(getApplicationContext())
//                                                            .setBitmap(resource);
                                                    wallpaperManager.setBitmap(resource, null, true,
                                                            WallpaperManager.FLAG_SYSTEM);
                                                    wallpaperManager.setBitmap(bitmap, null, true,
                                                            WallpaperManager.FLAG_LOCK);
                                                } catch (IOException e) {
                                                    e.printStackTrace();
                                                }
                                                determinateProgress.setVisibility(View.GONE);
                                                Snackbar.make(rootLayout, "Wallpaper was set to main and lock screen", Snackbar.LENGTH_SHORT).show();
                                            }
                                        });
                                break;
                        }
                    }

                });

            }
        });

    }


    public void checkPermissions(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (ContextCompat.checkSelfPermission(SetFromGalleryActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                Toast.makeText(SetFromGalleryActivity.this, "Permiso denegado", Toast.LENGTH_LONG).show();
                ActivityCompat.requestPermissions(SetFromGalleryActivity.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            } else {
                initImageCropper();
            }
        } else {
            initImageCropper();
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                filePath = result.getUri();

                GlideApp.with(SetFromGalleryActivity.this).load(filePath).fitCenter().into(previewImageView);
                setWallpaperButton.setEnabled(true);
                //uploadImage();

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {

                Exception error = result.getError();
                Toast.makeText(SetFromGalleryActivity.this, "Error en recortar la imagen.", Toast.LENGTH_SHORT).show();
            }
        }

    }

    private void initImageCropper() {
        //Set crop properties.
        //Just accept images cropped with Android device screen aspect ratio
        CropImage.activity()
                .setFixAspectRatio(true)
                .setAspectRatio(9,16)
                .setInitialCropWindowPaddingRatio(0)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMinCropResultSize(1920, 1080)
                .setActivityTitle("RECORTAR")
                .setCropMenuCropButtonTitle("OK")
                .start(this);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
