package com.nicolasrf.wallpaperapp.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.nicolasrf.wallpaperapp.Common.Common;
import com.nicolasrf.wallpaperapp.FavoritesActivity;
import com.nicolasrf.wallpaperapp.Helper.GlideApp;
import com.nicolasrf.wallpaperapp.Interface.ItemClickListener;
import com.nicolasrf.wallpaperapp.ListWallpaperActivity;
import com.nicolasrf.wallpaperapp.Model.CategoryItem;
import com.nicolasrf.wallpaperapp.Model.WallpaperItem;
import com.nicolasrf.wallpaperapp.MyUploadsActivity;
import com.nicolasrf.wallpaperapp.R;
import com.nicolasrf.wallpaperapp.RandomListActivity;
import com.nicolasrf.wallpaperapp.ViewHolder.ListWallpaperViewHolder;
import com.nicolasrf.wallpaperapp.ViewMyUploadActivity;
import com.nicolasrf.wallpaperapp.ViewWallpaperActivity;

import java.util.List;


public class WallpaperListAdapter extends RecyclerView.Adapter<ListWallpaperViewHolder> {
    private static final String TAG = "WallpaperListAdapter";

    private Context context;
    private List<WallpaperItem> wallpaperItems;
    private Intent intent;

    public WallpaperListAdapter(Context context, List<WallpaperItem> wallpaperItems) {
        this.context = context;
        this.wallpaperItems = wallpaperItems;
    }

    @NonNull
    @Override
    public ListWallpaperViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_wallpaper_item,parent,false);
//        int height = parent.getMeasuredHeight()/2;
//        itemView.setMinimumHeight(height);
        return new ListWallpaperViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ListWallpaperViewHolder holder, final int position) {

        GlideApp.with(context)
                .load(wallpaperItems.get(position).getImageUrl())
                .placeholder(R.drawable.placeholder)
                .apply(new RequestOptions().override(550, 700))
                .into(holder.wallpaperImageView);

        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int position) {

                //Check context of ActivityList for go to correct ViewActivity!
                if(context instanceof MyUploadsActivity){
                    intent = new Intent(context, ViewMyUploadActivity.class);
                    Common.CATEGORY_SELECTED = "MyUploads";
                }
                else if(context instanceof FavoritesActivity){
                    intent = new Intent(context, ViewWallpaperActivity.class);
                    Common.CATEGORY_SELECTED = "Favorites";
                }
                else if(context instanceof ListWallpaperActivity){
                    intent = new Intent(context, ViewWallpaperActivity.class);
                    //get category selected name.
                    FirebaseDatabase.getInstance().getReference(Common.STR_CATEGORY_BACKGROUND)
                            .child(wallpaperItems.get(position).getCategoryId())
                            .addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    CategoryItem categoryItem = dataSnapshot.getValue(CategoryItem.class);
                                    Common.CATEGORY_SELECTED = categoryItem.getName();
                                    //set
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });
                }

                else if(context instanceof RandomListActivity) {
                    intent = new Intent(context, ViewWallpaperActivity.class);
                    Common.CATEGORY_SELECTED = "Explore";
                }

                WallpaperItem wallpaperItem = new WallpaperItem(); //create Item
                //Setear todas las propiedades. En el FirebaseRecyclerAdapter venía implicito en el query.
                wallpaperItem.setId(wallpaperItems.get(position).getId());
                wallpaperItem.setCategoryId(wallpaperItems.get(position).getCategoryId());

                wallpaperItem.setImageThumb(wallpaperItems.get(position).getImageThumb());
                wallpaperItem.setImageUrl(wallpaperItems.get(position).getImageUrl());
                wallpaperItem.setReportedCount(wallpaperItems.get(position).getReportedCount());
                wallpaperItem.setTargetCount(wallpaperItems.get(position).getTargetCount());
                wallpaperItem.setViewCount(wallpaperItems.get(position).getViewCount());
                Common.select_background = wallpaperItem;
                Common.select_background_key = String.valueOf(wallpaperItems.get(position).getId());
                Log.d(TAG, "onClick: INSTANCE OF " + context);
                context.startActivity(intent);
            }
        });

    }

    private void removeFromBackgrounds() {
        FirebaseDatabase.getInstance().getReference(Common.STR_WALLPAPER)
                .child(Common.select_background_key)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if(dataSnapshot.exists()){
                            dataSnapshot.getRef().removeValue();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
        //dbFavs.removeValue();
    }

    @Override
    public int getItemCount() {
        return wallpaperItems.size();
    }
}
