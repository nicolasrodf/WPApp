package com.nicolasrf.wallpaperapp;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.constraint.Group;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.nicolasrf.wallpaperapp.Adapter.WallpaperListAdapter;
import com.nicolasrf.wallpaperapp.Common.Common;
import com.nicolasrf.wallpaperapp.Interface.ItemClickListener;
import com.nicolasrf.wallpaperapp.Model.WallpaperItem;
import com.nicolasrf.wallpaperapp.ViewHolder.ListWallpaperViewHolder;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class ListWallpaperActivity extends AppCompatActivity {
    private static final String TAG = "ListWallpaperActivity";

//    Query query;
//    FirebaseRecyclerOptions<WallpaperItem> options;
//    FirebaseRecyclerAdapter<WallpaperItem,ListWallpaperViewHolder> adapter;

    RecyclerView recyclerView;

    List<WallpaperItem> wallpaperItemList;
    List<String> mKeys = new ArrayList<>();
    WallpaperListAdapter adapter;

    Boolean isScrolling = false;
    int currentItems, totalItems, scrollOutItems;

    GridLayoutManager gridLayoutManager;

    ProgressBar recyclerProgressBar;
    ImageView noInternetImageView;
    Button retryButton;

    //int randomNumber;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_wallpaper);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(Common.CATEGORY_SELECTED);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        noInternetImageView = findViewById(R.id.no_internet_image);
        retryButton = findViewById(R.id.retry_button);

        wallpaperItemList = new ArrayList<>();
        recyclerProgressBar = findViewById(R.id.recycler_progress_bar);

        recyclerView = findViewById(R.id.wallpaper_list_recycler);
        recyclerView.setVisibility(View.GONE);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new DividerItemDecoration(this,
                DividerItemDecoration.HORIZONTAL));
        recyclerView.addItemDecoration(new DividerItemDecoration(this,
                DividerItemDecoration.VERTICAL));
        gridLayoutManager = new GridLayoutManager(this,2);
        recyclerView.setLayoutManager(gridLayoutManager);
        adapter = new WallpaperListAdapter(this,wallpaperItemList);

        //Comeince gone
        noInternetImageView.setVisibility(View.GONE);
        retryButton.setVisibility(View.GONE);

        //Check internet connection
        ConnectivityManager cm =
                (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        if(isConnected) {

            loadBackgroundList();
            setWallpaperList();
        }
        else {
            noInternetImageView.setVisibility(View.VISIBLE);
            retryButton.setVisibility(View.VISIBLE);
            recyclerProgressBar.setVisibility(View.GONE);

        }

        //Toast.makeText(getContext(), "You are not connected to internet", Toast.LENGTH_SHORT).show();
        retryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Check internet connection
                ConnectivityManager cm =
                        (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                boolean isConnected = activeNetwork != null &&
                        activeNetwork.isConnectedOrConnecting();
                if(isConnected) {
                    recyclerProgressBar.setVisibility(View.VISIBLE);
                    setWallpaperList();
                    loadBackgroundList();
                    noInternetImageView.setVisibility(View.GONE);
                    retryButton.setVisibility(View.GONE);
                }
                else {
                    noInternetImageView.setVisibility(View.VISIBLE);
                    retryButton.setVisibility(View.VISIBLE);
                    recyclerProgressBar.setVisibility(View.GONE);
                }
            }
        });

        //loadBackgroundList();
    }

    private void setWallpaperList() {
        recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
                {
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItems = gridLayoutManager.getChildCount();
                totalItems = gridLayoutManager.getItemCount();
                scrollOutItems = gridLayoutManager.findFirstVisibleItemPosition();

                if(isScrolling && (currentItems + scrollOutItems == totalItems))
                {
                    isScrolling = false;
                    //loadBackgroundList(); //Allow infinite scrolling!
                }
            }
        });
    }


    private void loadBackgroundList() {
        FirebaseDatabase.getInstance().getReference(Common.STR_WALLPAPER)
                .orderByChild("categoryId")
                .equalTo(Common.CATEGORY_ID_SELECTED)
                .addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                        WallpaperItem favoriteItem = dataSnapshot.getValue(WallpaperItem.class);
                        wallpaperItemList.add(favoriteItem); //ADD
                        //Common.select_background_key = dataSnapshot.getKey(); //KEY ATTACHED TO COMMON
                        String key = dataSnapshot.getKey();
                        mKeys.add(key);

                        recyclerProgressBar.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                        //Collections.shuffle(wallpaperItemList); //SET RANDOM!!
                        adapter.notifyDataSetChanged();

                        Log.d(TAG, "onChildAdded: ADDED " + key);
                    }

                    @Override
                    public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                        WallpaperItem favoriteItem = dataSnapshot.getValue(WallpaperItem.class);
                        String key = dataSnapshot.getKey();

                        int index = mKeys.indexOf(key);
                        wallpaperItemList.set(index,favoriteItem);

                        recyclerProgressBar.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);

                        adapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                        //WallpaperItem favoriteItem = dataSnapshot.getValue(WallpaperItem.class);
                        String key = dataSnapshot.getKey();

                        int index = mKeys.indexOf(key);
                        wallpaperItemList.remove(index);

                        recyclerProgressBar.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);

                        adapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home)
            finish(); //Close activity when click back button!
        return super.onOptionsItemSelected(item);
    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//        //Check internet connection
//        ConnectivityManager cm =
//                (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
//        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
//        boolean isConnected = activeNetwork != null &&
//                activeNetwork.isConnectedOrConnecting();
//        if(isConnected) {
//            recyclerProgressBar.setVisibility(View.VISIBLE);
//            //loadBackgroundList();
//            setWallpaperList();
//        }
//        else{
//            noInternetImageView.setVisibility(View.VISIBLE);
//            retryButton.setVisibility(View.VISIBLE);
//            recyclerProgressBar.setVisibility(View.INVISIBLE);
//        }
//    }
}
