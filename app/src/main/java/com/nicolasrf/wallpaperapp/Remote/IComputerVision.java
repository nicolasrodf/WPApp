package com.nicolasrf.wallpaperapp.Remote;

import com.nicolasrf.wallpaperapp.Model.AnalyzeModel.ComputerVision;
import com.nicolasrf.wallpaperapp.Model.AnalyzeModel.URLUpload;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface IComputerVision{

    @Headers({
            "Content-Type:application/json",
            "Ocp-Apim-Subscription-Key:d9e827d1e949443fb09d7e5a63606d77"
    })

    @POST
    Call<ComputerVision> analyzeImage(@Url String apiEndPoint, @Body URLUpload url);

}
