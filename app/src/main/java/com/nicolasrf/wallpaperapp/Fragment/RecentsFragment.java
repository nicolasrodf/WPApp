package com.nicolasrf.wallpaperapp.Fragment;


import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.constraint.Group;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.nicolasrf.wallpaperapp.Adapter.MyRecyclerAdapter;
import com.nicolasrf.wallpaperapp.Database.DataSource.RecentRepository;
import com.nicolasrf.wallpaperapp.Database.LocalDatabase.LocalDatabase;
import com.nicolasrf.wallpaperapp.Database.LocalDatabase.RecentsDataSource;
import com.nicolasrf.wallpaperapp.Database.Recents;
import com.nicolasrf.wallpaperapp.R;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class RecentsFragment extends Fragment {
    private static final String TAG = "RecentsFragment";

    private static RecentsFragment INSTANCE = null;

    RecyclerView recyclerView;
    Context context;

    List<Recents> recentsList;
    MyRecyclerAdapter adapter;

    //Room Database
    CompositeDisposable compositeDisposable;
    RecentRepository recentRepository;

    Group group;

    @SuppressLint("ValidFragment")
    public RecentsFragment(Context context) {
        this.context = context;
        //Init Roomtabase
        compositeDisposable = new CompositeDisposable();
        LocalDatabase database = LocalDatabase.getInstance(context);
        recentRepository = RecentRepository.getInstance(RecentsDataSource.getInstance(database.recentsDAO()));
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static RecentsFragment getInstance(Context context){
        if(INSTANCE == null)
            INSTANCE = new RecentsFragment(context);
        return INSTANCE;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_recents, container, false);
        recyclerView = view.findViewById(R.id.recents_recycler);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new DividerItemDecoration(context,
                DividerItemDecoration.HORIZONTAL));
        recyclerView.addItemDecoration(new DividerItemDecoration(context,
                DividerItemDecoration.VERTICAL));
        recyclerView.setHasFixedSize(true);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(),2);
        recyclerView.setLayoutManager(gridLayoutManager);
        recentsList = new ArrayList<>();
        adapter = new MyRecyclerAdapter(context,recentsList);
        //recyclerView.setAdapter(adapter);

        group = view.findViewById(R.id.group);
        group.setVisibility(View.GONE); //Comience Gone

        loadRecents();

        //Set adapter only if exist connection
        //Check internet connection
        ConnectivityManager cm =
                (ConnectivityManager)getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        if(isConnected)
            recyclerView.setAdapter(adapter);
        else
            group.setVisibility(View.VISIBLE);

        //Toast.makeText(getContext(), "You are not connected to internet", Toast.LENGTH_SHORT).show();

        Button retryButton = view.findViewById(R.id.retry_button);
        retryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Check internet connection
                ConnectivityManager cm =
                        (ConnectivityManager)getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                boolean isConnected = activeNetwork != null &&
                        activeNetwork.isConnectedOrConnecting();
                if(isConnected) {
                    recyclerView.setAdapter(adapter);
                    group.setVisibility(View.GONE);
                }
                else
                    group.setVisibility(View.VISIBLE);
            }
        });

        return view;

    }

    private void loadRecents() {
        Log.d(TAG, "loadRecents: STARTED");
        Disposable disposable = recentRepository.getAllRecents()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<List<Recents>>() {
                    @Override
                    public void accept(List<Recents> recents) throws Exception {
                        onGetAllRecentsSuccess(recents);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Log.d(TAG, "accept: " + throwable.getMessage());
                    }
                });
        compositeDisposable.add(disposable);
    }

    private void onGetAllRecentsSuccess(List<Recents> recents) {
        Log.d(TAG, "onGetAllRecentsSuccess: STARTED");
        recentsList.clear();
        recentsList.addAll(recents);
        Log.d(TAG, "onGetAllRecentsSuccess: SIZE " + recentsList.size());
        Log.d(TAG, "onGetAllRecentsSuccess: ADAPTER SIZE " + adapter.getItemCount());
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onDestroy() {
        compositeDisposable.clear();
        super.onDestroy();
    }

    @Override
    public void onResume() {
        Log.d(TAG, "onResume: RECENTS FRAGMENT");
        super.onResume();
        //Check internet connection
        ConnectivityManager cm =
                (ConnectivityManager)getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        if(isConnected)
            recyclerView.setAdapter(adapter);
        else
            group.setVisibility(View.VISIBLE);
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart: RECENTS FRAGMENT");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "onStop: RECENTS FRAGMENT");
    }
}
