package com.nicolasrf.wallpaperapp.Fragment;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.Group;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.nicolasrf.wallpaperapp.Common.Common;
import com.nicolasrf.wallpaperapp.Helper.GlideApp;
import com.nicolasrf.wallpaperapp.Interface.ItemClickListener;
import com.nicolasrf.wallpaperapp.Model.CategoryItem;
import com.nicolasrf.wallpaperapp.Model.WallpaperItem;
import com.nicolasrf.wallpaperapp.R;
import com.nicolasrf.wallpaperapp.ViewHolder.ListWallpaperViewHolder;
import com.nicolasrf.wallpaperapp.ViewWallpaperActivity;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;


public class TrendingFragment extends Fragment {
    private static final String TAG = "TrendingFragment";

    RecyclerView recyclerView;

    FirebaseDatabase database;
    DatabaseReference categoryBackground;

    FirebaseRecyclerOptions<WallpaperItem> options;
    FirebaseRecyclerAdapter<WallpaperItem,ListWallpaperViewHolder> adapter;

    private static TrendingFragment INSTANCE = null;

    Group group;


    public TrendingFragment() {
        // Init firebase
        database = FirebaseDatabase.getInstance();
        categoryBackground = database.getReference(Common.STR_WALLPAPER);

        Query query = categoryBackground.orderByChild("viewCount")
                .limitToLast(10); //limit to 10 most viewed

        options = new FirebaseRecyclerOptions.Builder<WallpaperItem>()
                .setQuery(query,WallpaperItem.class)
                .build();

        adapter = new FirebaseRecyclerAdapter<WallpaperItem, ListWallpaperViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull final ListWallpaperViewHolder holder, int position, @NonNull final WallpaperItem model) {

                GlideApp.with(getActivity())
                        .load(model.getImageUrl())
                        .placeholder(R.drawable.placeholder2)
                        .apply(new RequestOptions().override(1400, 700))
                        .into(holder.wallpaperImageView);

                holder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position) {

                        Intent intent = new Intent(getActivity(), ViewWallpaperActivity.class);
                        Common.select_background = model;
                        //get category selected name.
                        Common.CATEGORY_SELECTED = "Top Descargas";
                        Common.select_background_key = adapter.getRef(position).getKey(); //key for this wallpaper
                        startActivity(intent);
                    }
                });
            }

            @NonNull
            @Override
            public ListWallpaperViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_wallpaper_item, parent, false);
//                int height = parent.getMeasuredHeight() / 2;
//                itemView.setMinimumHeight(height);
                return new ListWallpaperViewHolder(itemView);
            }
        };
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static TrendingFragment getInstance(){
        if(INSTANCE == null)
            INSTANCE = new TrendingFragment();

        return INSTANCE;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_trending, container, false);
        recyclerView = view.findViewById(R.id.trending_recycler);
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        //Becasuee fierbase retirn ascending sort list so we need reverse RecyclerVierw to show largest item is first
        linearLayoutManager.setStackFromEnd(true);
        linearLayoutManager.setReverseLayout(true);
        recyclerView.setLayoutManager(linearLayoutManager);

        group = view.findViewById(R.id.group);
        group.setVisibility(View.GONE); //Comience Gone

        //Check internet connection
        ConnectivityManager cm =
                (ConnectivityManager)getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        if(isConnected) {
            recyclerView.setVisibility(View.INVISIBLE);
            loadTrendingList();
        }
        else {
            group.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.INVISIBLE);
        }

        //Toast.makeText(getContext(), "You are not connected to internet", Toast.LENGTH_SHORT).show();

        Button retryButton = view.findViewById(R.id.retry_button);
        retryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Check internet connection
                ConnectivityManager cm =
                        (ConnectivityManager)getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                boolean isConnected = activeNetwork != null &&
                        activeNetwork.isConnectedOrConnecting();
                if(isConnected) {
                    recyclerView.setVisibility(View.VISIBLE);
                    loadTrendingList();
                    group.setVisibility(View.GONE);
                }
                else {
                    group.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.INVISIBLE);
                }
            }
        });



        return view;
    }

    private void loadTrendingList() {
        adapter.startListening();
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onStop() {
        Log.d(TAG, "onStop: TRENDING FRAGMENT");
        if(adapter!=null)
            adapter.stopListening();
        super.onStop();
    }

    @Override
    public void onStart() {
        Log.d(TAG, "onStart: TRENDING FRAGMENT");
        super.onStart();
        if(adapter!=null) {
            adapter.startListening();
            //Check internet connection
            ConnectivityManager cm =
                    (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            boolean isConnected = activeNetwork != null &&
                    activeNetwork.isConnectedOrConnecting();
            if (isConnected) {
                group.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                loadTrendingList();
            } else {
                group.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.INVISIBLE);
            }
        }
    }

//    @Override
//    public void onResume() {
//        Log.d(TAG, "onResume: TRENDING FRAGMENT");
//        super.onResume();
//        if(adapter!=null) {
//            adapter.startListening();
//            //Check internet connection
//            ConnectivityManager cm =
//                    (ConnectivityManager)getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
//            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
//            boolean isConnected = activeNetwork != null &&
//                    activeNetwork.isConnectedOrConnecting();
//            if(isConnected) {
//                group.setVisibility(View.GONE);
//                recyclerView.setVisibility(View.VISIBLE);
//                loadTrendingList();
//            }
//            else {
//                group.setVisibility(View.VISIBLE);
//                recyclerView.setVisibility(View.INVISIBLE);
//            }
//        }
//    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        Log.d(TAG, "setUserVisibleHint: TRENDING FRAGMENT");
        if (isVisibleToUser) {
            // Do your stuff here
            if(adapter!=null) {
                adapter.startListening();
                //Check internet connection
                ConnectivityManager cm =
                        (ConnectivityManager)getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                boolean isConnected = activeNetwork != null &&
                        activeNetwork.isConnectedOrConnecting();
                if(isConnected) {
                    group.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                    loadTrendingList();
                }
                else {
                    group.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.INVISIBLE);
                }
            }
        }

    }
}
