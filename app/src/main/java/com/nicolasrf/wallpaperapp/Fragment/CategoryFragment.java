package com.nicolasrf.wallpaperapp.Fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.Group;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.nicolasrf.wallpaperapp.Common.Common;
import com.nicolasrf.wallpaperapp.Helper.GlideApp;
import com.nicolasrf.wallpaperapp.Interface.ItemClickListener;
import com.nicolasrf.wallpaperapp.ListWallpaperActivity;
import com.nicolasrf.wallpaperapp.Model.CategoryItem;
import com.nicolasrf.wallpaperapp.R;
import com.nicolasrf.wallpaperapp.ViewHolder.CategoryViewHolder;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

public class CategoryFragment extends Fragment {
    private static final String TAG = "CategoryFragment";

    //Firebase
    FirebaseDatabase database;
    DatabaseReference categoryBackground;
    //Firebase Ui Adapter
    FirebaseRecyclerOptions<CategoryItem> options;
    FirebaseRecyclerAdapter<CategoryItem,CategoryViewHolder> adapter;
    //View
    RecyclerView recyclerView;

    Group group;

    ProgressBar progressBar;


    private static CategoryFragment INSTANCE = null;


    public CategoryFragment() {
        database = FirebaseDatabase.getInstance();
        categoryBackground = database.getReference(Common.STR_CATEGORY_BACKGROUND);

        options = new FirebaseRecyclerOptions.Builder<CategoryItem>()
                .setQuery(categoryBackground,CategoryItem.class) //select all
                .build();

        adapter = new FirebaseRecyclerAdapter<CategoryItem, CategoryViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull final CategoryViewHolder holder, int position, @NonNull final CategoryItem model) {


                GlideApp.with(getActivity())
                        .load(model.getImageLink())
                        //.override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                        //.dontTransform()
                        .into(holder.backgroundImageView);

                holder.categoryNameTextView.setText(model.getName());
                holder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position) {
                        Common.CATEGORY_ID_SELECTED = adapter.getRef(position).getKey(); //get key for item
                        Common.CATEGORY_SELECTED = model.getName();
                        Intent intent = new Intent(getActivity(), ListWallpaperActivity.class);
                        startActivity(intent);
                    }
                });
            }

            @NonNull
            @Override
            public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_category_item,parent,false);
                return new CategoryViewHolder(itemView);
            }

            @Override
            public void onDataChanged() {
                super.onDataChanged();
                if (progressBar != null) {
                    progressBar.setVisibility(View.GONE);
                }
            }


        };
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static CategoryFragment getInstance(){
        if(INSTANCE == null)
            INSTANCE = new CategoryFragment();

        return INSTANCE;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_category, container, false);
        recyclerView = view.findViewById(R.id.category_recycler);
        recyclerView.setHasFixedSize(true);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(),2);
        recyclerView.setLayoutManager(gridLayoutManager);

        group = view.findViewById(R.id.group);
        group.setVisibility(View.GONE); //Comience Gone

        progressBar = view.findViewById(R.id.progress_bar);

        //Check internet connection
        ConnectivityManager cm =
                (ConnectivityManager)getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        if(isConnected)
            setCategory();
        else {
            group.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
        }

        //Toast.makeText(getContext(), "You are not connected to internet", Toast.LENGTH_SHORT).show();

        Button retryButton = view.findViewById(R.id.retry_button);
        retryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Check internet connection
                ConnectivityManager cm =
                        (ConnectivityManager)getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                boolean isConnected = activeNetwork != null &&
                        activeNetwork.isConnectedOrConnecting();
                if(isConnected) {
                    setCategory();
                    group.setVisibility(View.GONE);
                }
                else {
                    group.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                }
            }
        });

        return view;
    }


    private void setCategory() {
        adapter.startListening();
        recyclerView.setAdapter(adapter);
    }

    //Crtrl+O

    @Override
    public void onStart() {
        Log.d(TAG, "onStart: CATEGORY FRAGMENT");
        super.onStart();
        if(adapter!=null)
            adapter.startListening();

    }

    @Override
    public void onStop() {
        Log.d(TAG, "onStop: CATEGORY FRAGMENT");
        if(adapter!=null)
            adapter.stopListening();
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: CATEGORY FRAGMENT");
        if(adapter!=null) {
            adapter.startListening();
        }
    }
}
