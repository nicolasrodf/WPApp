package com.nicolasrf.wallpaperapp;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.nicolasrf.wallpaperapp.Adapter.WallpaperListAdapter;
import com.nicolasrf.wallpaperapp.Model.WallpaperItem;

import java.util.ArrayList;
import java.util.List;

public class FavoritesActivity extends AppCompatActivity {
    private static final String TAG = "FavoritesActivity";

    RecyclerView recyclerView;

    TextView emptyView;

    List<WallpaperItem> favoriteItemList;
    List<String> mKeys = new ArrayList<>();
    WallpaperListAdapter adapter;

    GridLayoutManager gridLayoutManager;

    Boolean isScrolling = false;
    int currentItems, totalItems, scrollOutItems;

    ProgressBar recyclerProgressBar;

    ImageView noInternetImageView;
    Button retryButton;


    //int randomNumber;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorites);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Favoritos");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

//        emptyView = findViewById(R.id.empty_view);

        favoriteItemList = new ArrayList<>();

        noInternetImageView = findViewById(R.id.no_internet_image);
        retryButton = findViewById(R.id.retry_button);

        //Comeince gone
        noInternetImageView.setVisibility(View.GONE);
        retryButton.setVisibility(View.GONE);

        recyclerProgressBar = findViewById(R.id.recycler_progress_bar);

        recyclerView = findViewById(R.id.favorites_recycler);
        recyclerView.setVisibility(View.GONE);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new DividerItemDecoration(this,
                DividerItemDecoration.HORIZONTAL));
        recyclerView.addItemDecoration(new DividerItemDecoration(this,
                DividerItemDecoration.VERTICAL));
        recyclerView.setHasFixedSize(true);
        gridLayoutManager = new GridLayoutManager(this,2);
        recyclerView.setLayoutManager(gridLayoutManager);
        adapter = new WallpaperListAdapter(this, favoriteItemList);

        //Check internet connection
        ConnectivityManager cm =
                (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        if(isConnected) {

            loadFavoriteList();
            setFavoriteList();
        }
        else {
            noInternetImageView.setVisibility(View.VISIBLE);
            retryButton.setVisibility(View.VISIBLE);
            recyclerProgressBar.setVisibility(View.GONE);

        }

        //Toast.makeText(getContext(), "You are not connected to internet", Toast.LENGTH_SHORT).show();
        retryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Check internet connection
                ConnectivityManager cm =
                        (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                boolean isConnected = activeNetwork != null &&
                        activeNetwork.isConnectedOrConnecting();
                if(isConnected) {
                    recyclerProgressBar.setVisibility(View.VISIBLE);
                    setFavoriteList();
                    loadFavoriteList();
                    noInternetImageView.setVisibility(View.GONE);
                    retryButton.setVisibility(View.GONE);
                }
                else {
                    noInternetImageView.setVisibility(View.VISIBLE);
                    retryButton.setVisibility(View.VISIBLE);
                    recyclerProgressBar.setVisibility(View.GONE);
                }
            }
        });


    }


    private void setFavoriteList() {
        recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
                {
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItems = gridLayoutManager.getChildCount();
                totalItems = gridLayoutManager.getItemCount();
                scrollOutItems = gridLayoutManager.findFirstVisibleItemPosition();

                if(isScrolling && (currentItems + scrollOutItems == totalItems))
                {
                    isScrolling = false;
                    //loadBackgroundList(); //Allow infinite scrolling!
                }
            }
        });
    }

    private void loadFavoriteList() {

        FirebaseDatabase.getInstance().getReference("users")
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child("favorites")
                .addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                        WallpaperItem favoriteItem = dataSnapshot.getValue(WallpaperItem.class);
                        favoriteItemList.add(favoriteItem); //ADD
                        //Common.select_background_key = dataSnapshot.getKey(); //KEY ATTACHED TO COMMON
                        String key = dataSnapshot.getKey();
                        mKeys.add(key);

                        recyclerProgressBar.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);

                        adapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                        WallpaperItem favoriteItem = dataSnapshot.getValue(WallpaperItem.class);
                        String key = dataSnapshot.getKey();

                        int index = mKeys.indexOf(key);
                        favoriteItemList.set(index,favoriteItem);

                        recyclerProgressBar.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);

                        adapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                        //WallpaperItem favoriteItem = dataSnapshot.getValue(WallpaperItem.class);
                        String key = dataSnapshot.getKey();

                        int index = mKeys.indexOf(key);
                        if(index<favoriteItemList.size()) {//to avoid errors
                            favoriteItemList.remove(index);
                            mKeys.remove(index);
                        }

                        recyclerProgressBar.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);

                        adapter.notifyItemRemoved(index);

                        adapter.notifyDataSetChanged();

                        Log.d(TAG, "onChildRemoved: REMOVED " + key);
                    }

                    @Override
                    public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home)
            finish(); //Close activity when click back button!
        return super.onOptionsItemSelected(item);
    }




}
